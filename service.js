(function () {
  angular
    .module('king.services.microphonepermission', [])
    .run(loadFunction);

  loadFunction.$inject = ['configService'];

  function loadFunction(configService) {
    // Register upper level modules
    try {
      if (configService.services && configService.services.microphonepermission) {
        askpermissionFunction(configService.services.microphonepermission.scope);
      } else {
        throw "The service is not added to the application";
      }
    } catch (error) {
      console.error("Error", error);
    }
  }

  function askpermissionFunction(scopeData) {
    if (!cordova) return;
    var deniedCount = 0;
   
    function onError(error) {
      console.error("The following error occurred: " + error);
    }

    function evaluateAuthorizationStatus(status) {
      switch (status) {
        case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
          console.log("Permission not requested");
          requestAuthorization();
          break;
        case cordova.plugins.diagnostic.permissionStatus.DENIED_ONCE:
          console.log("Permission denied");
          if (deniedCount < 3) {
            deniedCount++;
            requestAuthorization();
          } else {
            // Are we sure we want to hassle the user more than 3 times?
          }
          break;
        case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
          console.log("Permission permanently denied");
          navigator.notification.confirm(
            "This app has been denied access to your microphone and it really needs it function properly. Would you like to switch to the app settings page to allow access?",
            function (i) {
              if (i == 1) {
                cordova.plugins.diagnostic.switchToSettings();
              }
            }, "Microphone access denied", ['Yes', 'No']);
          break;
          case cordova.plugins.diagnostic.permissionStatus.GRANTED:
            console.log("Permission granted always");
            // Yay! use microphone
            break;
        
          default:
            console.log("out of cases. STATUS: "+ status)
      }
    }

    function requestAuthorization() {
      cordova.plugins.diagnostic.requestMicrophoneAuthorization(evaluateAuthorizationStatus, onError);
    }

    function checkAuthorization() {
      cordova.plugins.diagnostic.getMicrophoneAuthorizationStatus(evaluateAuthorizationStatus, onError);
    }

    checkAuthorization();

  }
  // --- End servicenameController content ---
})();